﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UcPag
{
    public class Pagamento
    {
        public static double GerarTaxajuro()
        {
            return 0.01;
        }
        public double CalcularJuros(double valorinicial, int periodo)
        {
            return valorinicial * Math.Pow(1 + GerarTaxajuro(), (double)periodo);
        }

    }

}

